Browser
=======

Zobrazuje jen adresáře

```csharp
        public BrowserForm()
        {
            InitializeComponent();

            treeView.ShowNodeToolTips = true;

            DriveInfo [] roots = DriveInfo.GetDrives ();
            foreach (DriveInfo r in roots)
               displayBranch (treeView.Nodes, r.RootDirectory);
        }
        
        void displayBranch (TreeNodeCollection target, DirectoryInfo dir, int levels = 2)
        {
            MyNode node = new MyNode ();
            node.path = dir.FullName;
            node.ready = false;
            node.Text = dir.Name;
            node.ToolTipText = node.path;
            node.ForeColor = Color.Blue;
            target.Add (node);
            displayDetail (node, dir, levels);
        }
        
        void displayDetail (MyNode node, DirectoryInfo dir, int levels)
        { 
            if (levels > 1)
                try  {
                    foreach (DirectoryInfo d in dir.GetDirectories())
                        displayBranch (node.Nodes, d, levels - 1);
                    node.ready = true;
                }
                catch (Exception e) {
                    TreeNode t = new TreeNode ();
                    t.Text = e.ToString ();
                    t.ToolTipText = e.StackTrace;
                    t.ForeColor = Color.Red;
                    node.Nodes.Add (t);
                }
        }
        
        private void treeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            foreach (TreeNode n in e.Node.Nodes)
            {
                MyNode node = n as MyNode;
                if (node != null && ! node.ready)
                {
                    try
                    {
                        DirectoryInfo dir = new DirectoryInfo(node.path);
                        displayDetail (node, dir, 2);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

```

![Browser.png](Pictures/Browser.png)
