﻿using System;
using System.Collections.Generic;
using System.Drawing; // Color

namespace Builder
{
    public class Record
    {
        [Connect ("TreeName")]
        public string RecordName { get; set; }

        [Connect ("TreeColor")]
        public Color RecordColor { get; set; }

        public static Color BasicColor () { return Color.Orange; }
    }
}
