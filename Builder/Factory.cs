﻿using System; // Activator
using System.Drawing; // Color
using System.Windows.Forms; // Button
using System.ComponentModel; // TypeDescriptor
using System.Reflection;  // MethodInfo

namespace Builder
{
    public class Factory : Button
    {
        public string ComponentName { get; set; }
        public Color ComponentColor { get; set; }
        public Type ComponentType { get; set; }
        public Type ViewType { get; set; }

        public Factory ()
        {
            this.Size = new Size (110, 46);
            this.MouseDown += this.Factory_MouseDown;
        }

        public Factory (Type type0, Type view0 = null, string name0 = "", Color color0 = default (Color)) : 
            this () // zavolat konstruktor bez parametru
        {
            ComponentType = type0;
            ViewType = view0;
            ComponentName = name0;
            ComponentColor = color0;

            if (ComponentName == "")
                ComponentName = ComponentType.Name;
            this.Text = ComponentName;

            ToolTip tooltip = new ToolTip ();
            tooltip.SetToolTip (this, ComponentName);

            // icon
            AttributeCollection attrCol = TypeDescriptor.GetAttributes (ComponentType);
            ToolboxBitmapAttribute imageAttr = attrCol [typeof (ToolboxBitmapAttribute)] as ToolboxBitmapAttribute;
            if (imageAttr != null)
            {
                this.Image = imageAttr.GetImage (ComponentType);
                this.Text = "";
                // this.TextAlign = ContentAlignment.MiddleRight;
            }

            MethodInfo meth = ComponentType.GetMethod ("BasicColor", BindingFlags.Public | BindingFlags.Static);
            if (meth != null)
            {
                Object result = meth.Invoke (null, null);
                this.BackColor = (Color)result;
            }
        }

        private void Factory_MouseDown (object sender, MouseEventArgs e)
        {
            DoDragDrop (this, DragDropEffects.Copy);
        }

        public virtual object createInstance ()
        {
            Object obj = Activator.CreateInstance (ComponentType);

            if (ViewType != null)
            {
                Object view = Activator.CreateInstance (ViewType);
                if (view is Common)
                {
                    Common com = view as Common;
                    com.setupData (obj);
                }
                obj = view; // return view object
            }

            return obj;
        }
    }
}
