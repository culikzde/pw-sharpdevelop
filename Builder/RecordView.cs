﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Builder
{
    public partial class RecordView : UserControl, Common
    {
        public RecordView ()
        {
            InitializeComponent ();
        }

        Record data;
        public  void setupData (object data0) { data =  data0 as Record; }
        public object recallData () { return data; }

        public override string ToString () { return "Record " + (data != null ? data.RecordName : "");  }
    }
}
