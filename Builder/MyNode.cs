﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Reflection;

namespace Builder
{
    public class MyNode : TreeNode
    {
        private PropertyDescriptor NameProp = null;
        private PropertyDescriptor ColorProp = null;

        public MyNode ()
        {
        }

        private object getData ()
        {
            object obj = this.Tag;
            if (obj is Common)
                obj = (obj as Common).recallData ();
            return obj;
        }

        private PropertyDescriptor findProperty (object obj, string connect_name, string default_name, string control_name)
        {
            // search property with Connection attribute and connection_name, store property name
            string property_name = "";
            Type type = obj.GetType ();
            foreach (PropertyInfo info in type.GetProperties ())
            {
                object [] attrs = info.GetCustomAttributes (typeof(ConnectAttribute), true);
                foreach (ConnectAttribute attr in attrs)
                   if (attr != null && attr.ConnectName == connect_name)
                       property_name = info.Name;
            }

            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties (obj);
            PropertyDescriptor result = null;

            // find property with connection Attribute
            if (property_name != "")
                result = properties.Find (property_name, false);

            // otherwise find property with default_name or control_name
            if (result == null)
                if (obj is Control)
                    result = properties.Find (control_name, false);
                else 
                    result = properties.Find (default_name, false);

            return result;
        }

        public void connect ()
        {
            object obj = getData ();

            NameProp = findProperty (obj, "TreeName", "Name", "Text");
            ColorProp = findProperty (obj, "TreeColor", "Color", "BackColor");

            if (NameProp != null)
                NameProp.AddValueChanged (obj, CopyName);
            if (ColorProp != null)
                ColorProp.AddValueChanged (obj, CopyColor);
        }

        private void CopyName (object sender, EventArgs e)
        {
            object obj = getData ();
 
            if (NameProp != null)
                this.Text = NameProp.GetValue (obj).ToString ();
        }

        private void CopyColor (object sender, EventArgs e)
        {
            object obj = getData ();

            if (ColorProp != null)
                this.BackColor = (Color)ColorProp.GetValue (obj);
        }
    }
}
