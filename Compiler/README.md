Compiler
========

:star: Solution Explorer, right click solution, Restore NuGet packages


```csharp
	var syntaxTree = CSharpSyntaxTree.ParseText(source);

	CSharpCompilation compilation = CSharpCompilation.Create(
		"assemblyName",
		new[] { syntaxTree },
		new[] { MetadataReference.CreateFromFile(typeof(object).Assembly.Location) },
		new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));

	using (var dllStream = new MemoryStream())
	using (var pdbStream = new MemoryStream())
	{
		var emitResult = compilation.Emit(dllStream, pdbStream);
		if (emitResult.Success)
			info("O.K.");
		else
			info("ERROR");
	}

	string code = syntaxTree.ToString();
	info(code);
	display(null, syntaxTree.GetRoot());
```

:star: Double click to show tree node properties

![Compiler.png](Pictures/Compiler.png)

https://stackoverflow.com/questions/32769630/how-to-compile-a-c-sharp-file-with-roslyn-programmatically