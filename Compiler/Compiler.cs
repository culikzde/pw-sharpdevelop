﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;

namespace Compiler
{
    public partial class Compiler : Form
    {
        public Compiler()
        {
            InitializeComponent();
        }

        private void info (string msg)
        {
            infoArea.AppendText(msg + "\r\n");
        }
        private void display (TreeNode target, SyntaxNode node)
        {
            TreeNode branch = new TreeNode();
            branch.Text = node.ToString();
            branch.Tag = node;

            if (target == null)
                treeView.Nodes.Add(branch);
            else
                target.Nodes.Add(branch);

            foreach (SyntaxNode item in node.ChildNodes())
                display(branch, item);
             
        }

        private void run_Click(object sender, EventArgs e)
        {
            string source = edit.Text;
            var syntaxTree = CSharpSyntaxTree.ParseText(source);

            CSharpCompilation compilation = CSharpCompilation.Create(
                "assemblyName",
                new[] { syntaxTree },
                new[] { MetadataReference.CreateFromFile(typeof(object).Assembly.Location) },
                new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));

            using (var dllStream = new MemoryStream())
            using (var pdbStream = new MemoryStream())
            {
                var emitResult = compilation.Emit(dllStream, pdbStream);
                if (emitResult.Success)
                    info("O.K.");
                else
                    info("ERROR");
            }

            string code = syntaxTree.ToString();
            info(code);
            display(null, syntaxTree.GetRoot());
        }

        private void treeView_DoubleClick(object sender, EventArgs e)
        {
        }

        private void treeView_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            propertyGrid.SelectedObject = e.Node.Tag;

        }
    }
}



// NuGet package Microsoft.CodeAnalysis.CSharp
// Solution Explorer, Compiler Project, References, Manage NuGet packages

// using System.IO;
// using Microsoft.CodeAnalysis;
// using Microsoft.CodeAnalysis.CSharp;

// https://stackoverflow.com/questions/32769630/how-to-compile-a-c-sharp-file-with-roslyn-programmatically


// New NuGet 
// https://www.nuget.org/downloads
// https://dist.nuget.org/win-x86-commandline/latest/nuget.exe
// nuget restore (in packages.config directory)
	