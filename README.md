
Příklady také pro Sharp Develop
===============================

[Draw](Draw) - bitmapové kreslení

[Browser](Browser) - obsah adresářů

[Attr](Attr) - metody s attributy, kontextové menu

[Builder](Builder) - drag and drop, custom attribure

[Properties](Properties) - property grid s využitím attributů, ukládaní do XML

Sharp Develop
=============

1. http://en.wikipedia.org/wiki/SharpDevelop
2. http://github.com/icsharpcode
3. http://sourceforge.net/projects/sharpdevelop/



README viewer
=============

http://addons.mozilla.org/cs/firefox/addon/gitlab-markdown-viewer

http://github.com/painyeph/GitLabMarkdownViewer