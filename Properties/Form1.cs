﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using System.Xml.Serialization;

namespace Properties
{
    public partial class Form1 : Form
    {
        Data data;

        public Form1()
        {
            InitializeComponent();

            data = new Data ();

            data.Name = "abc";
            data.X = 1;
            data.Y = 2;
            data.Z = data.Y + 1;

            propertyGrid1.SelectedObject = data;

            // using System.IO;
            // using System.Xml.Serialization;
            StringWriter w = new StringWriter ();
            XmlSerializer t = new XmlSerializer (typeof (Data));
            t.Serialize(w, data);
            string s = w.ToString();
            textBox1.AppendText(s + "\n\r");

        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog () == DialogResult.OK)
            {
                StreamWriter w = new StreamWriter(saveFileDialog1.FileName);
                XmlSerializer t = new XmlSerializer(typeof(Data));
                t.Serialize(w, data);
                w.Close();
            }
        }

        private void Open_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamReader r = new StreamReader (openFileDialog1.FileName);
                XmlSerializer t = new XmlSerializer(typeof(Data));
                data = (Data) t.Deserialize (r);
                propertyGrid1.SelectedObject = data;
                r.Close();
            }
        }
    }
}
