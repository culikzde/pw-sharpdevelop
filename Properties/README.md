Properties
==========

```csharp
    using System.Drawing;
    using System.ComponentModel;
    
    public class Data // public ... important
    {
        [DisplayName("Jmeno")]
        [Description("Jmeno objektu")]
        public string Name { get; set; }

        [Category ("Souradnice")]  
        public int X { get; set;  }

        [Category("Souradnice")]
        public int Y { get; set; }

        [Category("Souradnice")]
        public int Z { get; set; }

        public Color SomeColor { get; set; }

        // [Browsable(false)]
        public bool OK { get; set; }

        public enum Shape {  line, rectangle, ellipse };
        public Shape SomeShape { get; set; }

        public Size EntrySize { get; set; }

        public Data ()
        {
            SomeColor = Color.Orange; // null values ... no edit
            SomeShape = Shape.line;
            EntrySize = new Size();
        }
    }
```

```csharp
    propertyGrid1.SelectedObject = new Data ();
```

```csharp
	using System.IO;
	using System.Xml.Serialization;

	if (openFileDialog1.ShowDialog() == DialogResult.OK)
	{
		StreamReader r = new StreamReader (openFileDialog1.FileName);
		XmlSerializer t = new XmlSerializer(typeof(Data));
		data = (Data) t.Deserialize (r);
		propertyGrid1.SelectedObject = data;
		r.Close();
	}
	
	if (saveFileDialog1.ShowDialog () == DialogResult.OK)
	{
		StreamWriter w = new StreamWriter(saveFileDialog1.FileName);
		XmlSerializer t = new XmlSerializer(typeof(Data));
		t.Serialize(w, data);
		w.Close();
	}
```

![Properties.png](Pictures/Properties.png)

