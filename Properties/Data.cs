﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.ComponentModel;

namespace Properties
{
    public class Data // public ... important
    {
        [DisplayName("Jmeno")]
        [Description("Jmeno objektu")]
        public string Name { get; set; }

        [Category ("Souradnice")]  
        public int X { get; set;  }

        [Category("Souradnice")]
        public int Y { get; set; }

        [Category("Souradnice")]
        public int Z { get; set; }

        public Color SomeColor { get; set; }

        // [Browsable(false)]
        public bool OK { get; set; }

        public enum Shape {  line, rectangle, ellipse };
        public Shape SomeShape { get; set; }

        public Size EntrySize { get; set; }

        public Data ()
        {
            SomeColor = Color.Orange; // null values ... no edit
            SomeShape = Shape.line;
            EntrySize = new Size();
        }

    }
}
