﻿namespace Attr
{
    partial class AttrForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
        	this.components = new System.ComponentModel.Container();
        	this.button1 = new System.Windows.Forms.Button();
        	this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
        	this.helloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        	this.mainMenu = new System.Windows.Forms.MenuStrip();
        	this.info = new System.Windows.Forms.TextBox();
        	this.contextMenu.SuspendLayout();
        	this.SuspendLayout();
        	// 
        	// button1
        	// 
        	this.button1.ContextMenuStrip = this.contextMenu;
        	this.button1.Location = new System.Drawing.Point(48, 42);
        	this.button1.Name = "button1";
        	this.button1.Size = new System.Drawing.Size(75, 23);
        	this.button1.TabIndex = 0;
        	this.button1.Text = "button1";
        	this.button1.UseVisualStyleBackColor = true;
        	this.button1.Click += new System.EventHandler(this.button1_Click);
        	// 
        	// contextMenu
        	// 
        	this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.helloToolStripMenuItem});
        	this.contextMenu.Name = "contextMenuStrip1";
        	this.contextMenu.Size = new System.Drawing.Size(153, 48);
        	this.contextMenu.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuOpening);
        	// 
        	// helloToolStripMenuItem
        	// 
        	this.helloToolStripMenuItem.Name = "helloToolStripMenuItem";
        	this.helloToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
        	this.helloToolStripMenuItem.Text = "Hello";
        	// 
        	// mainMenu
        	// 
        	this.mainMenu.Location = new System.Drawing.Point(0, 0);
        	this.mainMenu.Name = "mainMenu";
        	this.mainMenu.Size = new System.Drawing.Size(800, 24);
        	this.mainMenu.TabIndex = 1;
        	this.mainMenu.Text = "menuStrip1";
        	// 
        	// info
        	// 
        	this.info.Location = new System.Drawing.Point(43, 137);
        	this.info.Multiline = true;
        	this.info.Name = "info";
        	this.info.Size = new System.Drawing.Size(505, 217);
        	this.info.TabIndex = 3;
        	// 
        	// AttrForm
        	// 
        	this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        	this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        	this.ClientSize = new System.Drawing.Size(800, 450);
        	this.Controls.Add(this.info);
        	this.Controls.Add(this.button1);
        	this.Controls.Add(this.mainMenu);
        	this.MainMenuStrip = this.mainMenu;
        	this.Name = "AttrForm";
        	this.Text = "Attr";
        	this.contextMenu.ResumeLayout(false);
        	this.ResumeLayout(false);
        	this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem helloToolStripMenuItem;
        private System.Windows.Forms.TextBox info;
        private System.Windows.Forms.MenuStrip mainMenu;
    }
}

