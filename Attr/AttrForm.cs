﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.ComponentModel;
using System.Reflection;


namespace Attr
{
    public partial class AttrForm : Form
    {
        public AttrForm()
        {
            InitializeComponent();
        }

        // using System.ComponentModel;
        [Description("Uvitani")]
        public string hello ()
        {
            return "Ahoj";
        }

        [Description ("Pokus")]
        public void test ()
        {
            button1.Text = "Test";
        }
        
        [Description("Secti dve cisla")]
        public int add ( int a, int b)
        {
            return a+b;
        }

        // using System.ComponentModel;
        [Description ("Vytiskni radku") ]
        public void put (string s)
        {
            info.AppendText(s + "\r\n");
        }
        
       private void button1_Click(object sender, EventArgs e)
        {
            info.Clear();
            Type t = this.GetType();
            // using System.Reflection;
            foreach (MethodInfo m in t.GetMethods ())
            {
                foreach (DescriptionAttribute a in m.GetCustomAttributes<DescriptionAttribute>())
                {
                    contextMenu.Items.Add (m.Name  + " " + a.Description);
                    put (m.Name  + " " + a.Description);
                }
            }
        }
       
		void ContextMenuOpening (object sender, CancelEventArgs e)
		{
            contextMenu.Items.Clear();
            object obj = this;
            // contextMenu.Items.Add("opening " + obj.ToString());

            Type t = obj.GetType();
            MethodInfo[] methods = t.GetMethods();
            foreach (MethodInfo m in methods)
            {
                DescriptionAttribute a = m.GetCustomAttribute<DescriptionAttribute>();
                if (a != null)
                {
                    ParameterInfo[] args = m.GetParameters();
                    if (args.Length == 0)
                    {
                        MyMenuItem item = new MyMenuItem ();
                        item.Text = m.Name + " ... " + a.Description;
                        item.obj = obj;
                        item.m = m;
                        item.Click += ContextMenuClick;
                        contextMenu.Items.Add(item);
                    }
                }
            }
		}
        private void ContextMenuClick(object sender, EventArgs e)
        {
        	MyMenuItem item = sender as MyMenuItem;
        	object result = item.m.Invoke(item.obj, new object[] { });
        	if (result == null) result = "null";
        	put("MENU CLICK ... " + result.ToString());
        }
    }
    
    class MyMenuItem : ToolStripMenuItem
    {
        public object obj;
        public MethodInfo m;

    };

}
