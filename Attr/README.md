Attr
====

```csharp
        // using System.ComponentModel;
        [Description("Uvitani")]
        public string hello ()
        {
            return "Ahoj";
        }

```

```csharp
        void ContextMenuOpening (object sender, CancelEventArgs e)
        {
            contextMenu.Items.Clear();
            object obj = this;
            // contextMenu.Items.Add("opening " + obj.ToString());

            Type t = obj.GetType();
            MethodInfo[] methods = t.GetMethods();
            foreach (MethodInfo m in methods)
            {
                DescriptionAttribute a = m.GetCustomAttribute<DescriptionAttribute>();
                if (a != null)
                {
                    ParameterInfo[] args = m.GetParameters();
                    if (args.Length == 0)
                    {
                        MyMenuItem item = new MyMenuItem ();
                        item.Text = m.Name + " ... " + a.Description;
                        item.obj = obj;
                        item.m = m;
                        item.Click += ContextMenuClick;
                        contextMenu.Items.Add(item);
                    }
                }
            }
        }
```		
```csharp
        private void ContextMenuClick(object sender, EventArgs e)
        {
        	MyMenuItem item = sender as MyMenuItem;
        	object result = item.m.Invoke(item.obj, new object[] { });
        	if (result == null) result = "null";
        	put("MENU CLICK ... " + result.ToString());
        }

```
```csharp
    class MyMenuItem : ToolStripMenuItem
    {
        public object obj;
        public MethodInfo m;

    };
```

![Attr.png](Pictures/Attr.png)
