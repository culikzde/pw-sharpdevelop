Draw
====

Vytvoreni/zvetseni obrazku
```csharp
	private void PictureBox_SizeChanged(object sender, EventArgs e)
	{
		Bitmap old = null;
		if (pictureBox.Image != null)
		   old = new Bitmap (pictureBox.Image);

		int w = pictureBox.Width;
		int h = pictureBox.Height;
		if (old != null)
		{
			if (old.Width > w) w = old.Width;
			if (old.Height > h) h = old.Height;
		}

		Bitmap b = new Bitmap (w, h);
		pictureBox.Image = b;
		Graphics g = Graphics.FromImage(pictureBox.Image);
		Brush br = new SolidBrush(Color.White);
		g.FillRectangle(br, 0, 0, w, h);

		if (old != null)
		   g.DrawImage (old, 0, 0);

		pictureBox.Invalidate();
	}
```

Pridani barevneho panelu
```csharp
	private List <Panel> colorPanels = new List<Panel> ();

	private void addColorPanel(Color c)
	{
		int colorPanelCount = colorPanels.Count;
		int x = colorPanelCount * (colorPanel.Left + colorPanel.Width) + colorPanel.Left;
		Panel p = new Panel();
		p.Left = x;
		p.Top = colorPanel.Top;
		p.Width = colorPanel.Width;
		p.Height = colorPanel.Height;
		p.BackColor = c;
		p.MouseDown += this.colorPanel_MouseDown;
		// p.Parent = toolPanel;
		toolPanel.Controls.Add(p);
		colorPanels.Add (p);
	}
```

Cteni ze souboru
```csharp
	if (openDialog.ShowDialog() == DialogResult.OK)
	{
		pictureBox.Image = new Bitmap(openDialog.FileName);
		PictureBox_SizeChanged(null, null);
	}

	// using System.Drawing.Imaging;
	if (saveDialog.ShowDialog() == DialogResult.OK)
		pictureBox.Image.Save(saveDialog.FileName, ImageFormat.Jpeg);
```


![Draw.png](Pictures/Draw.png)

- levým tlačítkem myši - barva okraje
- pravým tlačítkem - barva výplně
- ctrl + pravé tlačítko - druhá barva výplně

```csharp
	// using System.Drawing.Drawing2D;
	int a = (int)numericUpDown1.Value;
	int b = (int)numericUpDown2.Value;
	brush = new LinearGradientBrush
			(new Point(0, 0),
			 new PointF(a, b),
			 brushColor,
			 panel.BackColor);

```
