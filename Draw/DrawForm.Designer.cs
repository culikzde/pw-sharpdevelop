﻿namespace Draw
{
	partial class DrawForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.pictureBox = new System.Windows.Forms.PictureBox();
			this.maninMenu = new System.Windows.Forms.MenuStrip();
			this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.openMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.quitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
			this.addColorButtonMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openDialog = new System.Windows.Forms.OpenFileDialog();
			this.saveDialog = new System.Windows.Forms.SaveFileDialog();
			this.colorDialog = new System.Windows.Forms.ColorDialog();
			this.toolPanel = new System.Windows.Forms.Panel();
			this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.comboBox = new System.Windows.Forms.ComboBox();
			this.colorPanel = new System.Windows.Forms.Panel();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
			this.maninMenu.SuspendLayout();
			this.toolPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			this.SuspendLayout();
			// 
			// pictureBox1
			// 
			this.pictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.pictureBox.Location = new System.Drawing.Point(0, 60);
			this.pictureBox.Name = "pictureBox1";
			this.pictureBox.Size = new System.Drawing.Size(800, 390);
			this.pictureBox.TabIndex = 0;
			this.pictureBox.TabStop = false;
			this.pictureBox.SizeChanged += new System.EventHandler(this.PictureBox_SizeChanged);
			this.pictureBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseDown);
			this.pictureBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseMove);
			this.pictureBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PictureBox_MouseUp);
			// 
			// menuStrip1
			// 
			this.maninMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.fileMenu,
			this.editMenu});
			this.maninMenu.Location = new System.Drawing.Point(0, 0);
			this.maninMenu.Name = "menuStrip1";
			this.maninMenu.Size = new System.Drawing.Size(800, 24);
			this.maninMenu.TabIndex = 1;
			this.maninMenu.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.openMenuItem,
			this.saveMenuItem,
			this.quitMenuItem});
			this.fileMenu.Name = "fileToolStripMenuItem";
			this.fileMenu.Size = new System.Drawing.Size(37, 20);
			this.fileMenu.Text = "&File";
			// 
			// openToolStripMenuItem
			// 
			this.openMenuItem.Name = "openToolStripMenuItem";
			this.openMenuItem.Size = new System.Drawing.Size(152, 22);
			this.openMenuItem.Text = "&Open";
			this.openMenuItem.Click += new System.EventHandler(this.Open_Click);
			// 
			// saveToolStripMenuItem
			// 
			this.saveMenuItem.Name = "saveToolStripMenuItem";
			this.saveMenuItem.Size = new System.Drawing.Size(152, 22);
			this.saveMenuItem.Text = "&Save";
			this.saveMenuItem.Click += new System.EventHandler(this.Save_Click);
			// 
			// quitToolStripMenuItem
			// 
			this.quitMenuItem.Name = "quitToolStripMenuItem";
			this.quitMenuItem.Size = new System.Drawing.Size(152, 22);
			this.quitMenuItem.Text = "&Quit";
			this.quitMenuItem.Click += new System.EventHandler(this.Quit_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
			this.addColorButtonMenuItem});
			this.editMenu.Name = "editToolStripMenuItem";
			this.editMenu.Size = new System.Drawing.Size(39, 20);
			this.editMenu.Text = "&Edit";
			// 
			// addColorButtonToolStripMenuItem
			// 
			this.addColorButtonMenuItem.Name = "addColorButtonToolStripMenuItem";
			this.addColorButtonMenuItem.Size = new System.Drawing.Size(158, 22);
			this.addColorButtonMenuItem.Text = "add &Color Panel";
			this.addColorButtonMenuItem.Click += new System.EventHandler(this.AddColorButton_Click);
			// 
			// openFileDialog1
			// 
			this.openDialog.FileName = "openFileDialog1";
			// 
			// toolPanel
			// 
			this.toolPanel.Controls.Add(this.numericUpDown2);
			this.toolPanel.Controls.Add(this.numericUpDown1);
			this.toolPanel.Controls.Add(this.comboBox);
			this.toolPanel.Controls.Add(this.colorPanel);
			this.toolPanel.Dock = System.Windows.Forms.DockStyle.Top;
			this.toolPanel.Location = new System.Drawing.Point(0, 24);
			this.toolPanel.Name = "toolPanel";
			this.toolPanel.Size = new System.Drawing.Size(800, 36);
			this.toolPanel.TabIndex = 2;
			// 
			// numericUpDown2
			// 
			this.numericUpDown2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.numericUpDown2.Location = new System.Drawing.Point(569, 10);
			this.numericUpDown2.Maximum = new decimal(new int[] {
			1000,
			0,
			0,
			0});
			this.numericUpDown2.Name = "numericUpDown2";
			this.numericUpDown2.Size = new System.Drawing.Size(79, 20);
			this.numericUpDown2.TabIndex = 3;
			this.numericUpDown2.Value = new decimal(new int[] {
			100,
			0,
			0,
			0});
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.numericUpDown1.Location = new System.Drawing.Point(484, 9);
			this.numericUpDown1.Maximum = new decimal(new int[] {
			1000,
			0,
			0,
			0});
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(79, 20);
			this.numericUpDown1.TabIndex = 2;
			this.numericUpDown1.Value = new decimal(new int[] {
			100,
			0,
			0,
			0});
			// 
			// comboBox1
			// 
			this.comboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox.FormattingEnabled = true;
			this.comboBox.Items.AddRange(new object[] {
			"line",
			"restangle",
			"ellipse"});
			this.comboBox.Location = new System.Drawing.Point(667, 9);
			this.comboBox.Name = "comboBox1";
			this.comboBox.Size = new System.Drawing.Size(121, 21);
			this.comboBox.TabIndex = 1;
			// 
			// colorPanel
			// 
			this.colorPanel.BackColor = System.Drawing.Color.Blue;
			this.colorPanel.Location = new System.Drawing.Point(3, 4);
			this.colorPanel.Name = "colorPanel";
			this.colorPanel.Size = new System.Drawing.Size(40, 26);
			this.colorPanel.TabIndex = 0;
			this.colorPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.colorPanel_MouseDown);
			// 
			// DrawForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.pictureBox);
			this.Controls.Add(this.toolPanel);
			this.Controls.Add(this.maninMenu);
			this.MainMenuStrip = this.maninMenu;
			this.Name = "DrawForm";
			this.Text = "Draw";
			((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
			this.maninMenu.ResumeLayout(false);
			this.maninMenu.PerformLayout();
			this.toolPanel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.MenuStrip maninMenu;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem openMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitMenuItem;
        private System.Windows.Forms.OpenFileDialog openDialog;
        private System.Windows.Forms.SaveFileDialog saveDialog;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Panel toolPanel;
        private System.Windows.Forms.Panel colorPanel;
        private System.Windows.Forms.ComboBox comboBox;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem addColorButtonMenuItem;
	}
}
