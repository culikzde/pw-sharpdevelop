﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace Draw
{
	public partial class DrawForm : Form
	{
        private Pen pen;
        private Brush brush;
        private Color brushColor;
        
        private enum Shape { line, rectangle, ellipse};

        // const int line = 0;
        // const int rectangle = 1;
        // const int ellipse = 2;
        
		public DrawForm()
        {
            InitializeComponent();

            PictureBox_SizeChanged(null, null);

            comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox.SelectedIndex = (int) Shape.line;
            
            colorPanels.Add (colorPanel); // original color panel

            Color[] colors = { Color.Red, Color.Orange, Color.Lime };
            foreach (Color c in colors)
                addColorPanel(c); // additional color panels

            pen = new Pen(colorPanels[0].BackColor);

            brushColor = colorPanels[1].BackColor;
            brush = new SolidBrush(brushColor);
        }

        private void PictureBox_SizeChanged(object sender, EventArgs e)
        {
            Bitmap old = null;
            if (pictureBox.Image != null)
               old = new Bitmap (pictureBox.Image);

            int w = pictureBox.Width;
            int h = pictureBox.Height;
            if (old != null)
            {
                if (old.Width > w) w = old.Width;
                if (old.Height > h) h = old.Height;
            }

            Bitmap b = new Bitmap (w, h);
            pictureBox.Image = b;
            Graphics g = Graphics.FromImage(pictureBox.Image);
            Brush br = new SolidBrush(Color.White);
            g.FillRectangle(br, 0, 0, w, h);

            if (old != null)
               g.DrawImage (old, 0, 0);

            pictureBox.Invalidate();
        }

        private int X0, Y0;
        private bool press = false;
        private Image save;

        private void PictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            X0 = e.X;
            Y0 = e.Y;
            press = true;
            save = new Bitmap (pictureBox.Image);
        }

		private void PictureBox_MouseMove(object sender, MouseEventArgs e)
		{
            if (press)
            {
                Graphics g = Graphics.FromImage(pictureBox.Image);
                g.DrawImage (save, 0, 0);

                Shape inx = (Shape) comboBox.SelectedIndex;
                if (inx == Shape.line)
                {
                    g.DrawLine(pen, X0, Y0, e.X, e.Y);
                }
                else if (inx == Shape.rectangle)
                {
                    int X1 = X0;
                    int Y1 = Y0;
                    int X2 = e.X;
                    int Y2 = e.Y;
                    if (X2 < X1) { int t = X1; X1 = X2; X2 = t; }
                    if (Y2 < Y1) { int t = Y1; Y1 = Y2; Y2 = t; }
					g.FillRectangle(brush, X1, Y1, X2 - X1, Y2 - Y1);
					g.DrawRectangle(pen, X1, Y1, X2 - X1, Y2 - Y1);
				}
                else if (inx == Shape.ellipse)
                {
                    g.FillEllipse(brush, X0, Y0, e.X - X0, e.Y - Y0);
                    g.DrawEllipse(pen, X0, Y0, e.X - X0, e.Y - Y0);
                }
                pictureBox.Invalidate();
            }
        }

        private void PictureBox_MouseUp(object sender, MouseEventArgs e)
        {
            press = false;
        }

        // private int colorPanelCount = 1;
        private List <Panel> colorPanels = new List<Panel> ();
	
        private void addColorPanel(Color c)
        {
        	int colorPanelCount = colorPanels.Count;
            int x = colorPanelCount * (colorPanel.Left + colorPanel.Width) + colorPanel.Left;
            Panel p = new Panel();
            p.Left = x;
            p.Top = colorPanel.Top;
            p.Width = colorPanel.Width;
            p.Height = colorPanel.Height;
            p.BackColor = c;
            p.MouseDown += this.colorPanel_MouseDown;
            // p.Parent = toolPanel;
            toolPanel.Controls.Add(p);
            colorPanels.Add (p);
            // colorPanelCount++;
        }

        private void colorPanel_MouseDown(object sender, MouseEventArgs e)
        {
            // Panel panel = (Panel) sender;
            Panel panel = sender as Panel;
            if (e.Button == MouseButtons.Left) 
            {
                pen = new Pen (panel.BackColor);
            }
            else if (e.Button == MouseButtons.Right)
            {
                if (Control.ModifierKeys != Keys.Control)
                {
                    brushColor = panel.BackColor;
                    brush = new SolidBrush(brushColor);
                }
                else
                {
                    // using System.Drawing.Drawing2D;
                    int a = (int)numericUpDown1.Value;
                    int b = (int)numericUpDown2.Value;
                    brush = new LinearGradientBrush
                            (new Point(0, 0),
                             new PointF(a, b),
                             brushColor,
                             panel.BackColor);
                }
            }
            else if (e.Button == MouseButtons.Middle)
            {
                colorDialog.Color = panel.BackColor;
                if (colorDialog.ShowDialog () == DialogResult.OK)
                {
                    panel.BackColor = colorDialog.Color;
                    pen = new Pen (panel.BackColor);
                }
            }
        }

        private void Open_Click(object sender, EventArgs e)
        {
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                pictureBox.Image = new Bitmap(openDialog.FileName);
                PictureBox_SizeChanged(null, null);
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
            // using System.Drawing.Imaging;
            if (saveDialog.ShowDialog() == DialogResult.OK)
                pictureBox.Image.Save(saveDialog.FileName, ImageFormat.Jpeg);
        }

        private void AddColorButton_Click(object sender, EventArgs e)
        {
            addColorPanel(Color.Yellow);
        }

        private void Quit_Click(object sender, EventArgs e)
        {
            Close();
        }
		
	}
}
