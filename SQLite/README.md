﻿### Sharp Develop

1. http://en.wikipedia.org/wiki/SharpDevelop
2. http://github.com/icsharpcode
3. http://sourceforge.net/projects/sharpdevelop/

### DB Browser for SQLite

- http://sqlitebrowser.org
- https://download.sqlitebrowser.org/DB.Browser.for.SQLite-3.11.2-win64.zip

### SQLite

Sharp Develop --> Menu Project --> Manage Packages 

nainstalovat balicky

```
System.Data.SQLite
SQLite.Interop
```
 
zkopirovat packages/SQLite.Interop.1.0.0/lib/SQLite.Interop.dll do adresare k .EXE souboru

```C#
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using System.Data.SQLite;

namespace pokus
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
		}
		
		void Button1Click(object sender, EventArgs e)
		{
			string db_path = "c:\\temp\\data.sqlite";
            SQLiteConnection.CreateFile(db_path);
			SQLiteConnection conn = new SQLiteConnection("Data Source=" + db_path + ";Version=3;");
            conn.Open();
            SQLiteCommand cmd;
            cmd = new SQLiteCommand ("CREATE TABLE nameindex (name TEXT, id INTEGER)", conn);
            cmd.ExecuteNonQuery();
            conn.Dispose();
		}
	}
}
```
